# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .factory.Factory import DeltaFactory


class DeltaGridView(Gtk.GridView, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.GridView.__init__(
            self,
            model=self._enquiry("delta > model"),
            factory=DeltaFactory(self),
            max_columns=999,
            )
        self.add_css_class("kusozako-content-area")
        scrolled_window.set_child(self)
        self._raise("delta > add to container end", scrolled_window)

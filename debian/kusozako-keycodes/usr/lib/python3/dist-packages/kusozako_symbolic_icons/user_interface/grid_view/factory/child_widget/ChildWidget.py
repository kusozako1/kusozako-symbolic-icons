# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .Image import DeltaImage
from .Label import DeltaLabel


class DeltaChildWidget(Gtk.AspectFrame, DeltaEntity):

    def bind(self, file_info):
        self._image.bind(file_info)
        self._label.bind(file_info)

    def _delta_call_add_to_container(self, widget):
        self._overlay.set_child(widget)

    def _delta_call_add_overlay(self, widget):
        self._overlay.add_overlay(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.AspectFrame.__init__(self, ratio=1, margin_top=8, margin_start=8)
        self._overlay = Gtk.Overlay()
        self.set_child(self._overlay)
        self._image = DeltaImage(self)
        self._label = DeltaLabel(self)
        self._overlay.add_css_class("card")

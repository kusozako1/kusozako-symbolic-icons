# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaParser(DeltaEntity):

    @classmethod
    def new(cls, parent, path):
        instance = cls(parent)
        instance.construct(path)

    def _dispatch(self, file_info, fullpath):
        if file_info.get_file_type() == Gio.FileType.DIRECTORY:
            self._raise("delta > directory found", fullpath)
        else:
            user_data = fullpath, file_info
            self._raise("delta > file found", user_data)

    def construct(self, path):
        gfile = Gio.File.new_for_path(path)
        enumerator = gfile.enumerate_children("*", 0)
        for file_info in enumerator:
            child_gfile = enumerator.get_child(file_info)
            fullpath = child_gfile.get_path()
            self._dispatch(file_info, fullpath)

    def __init__(self, parent):
        self._parent = parent

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

CATEGORY_CHANGED = "category-changed"            # category as string
QUERY_KEYWORD_CHANGED = "query-keyword-changed"  # keyword as string

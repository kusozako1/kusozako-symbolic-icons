# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .CategoryModel import DeltaCategoryModel
from .base_model.BaseModel import DeltaBaseModel
from .filter_model.FilterModel import DeltaFilterModel


class DeltaModels(DeltaEntity):

    def _delta_call_category_found(self, category):
        self._category_model.append_category(category)

    def get_category_model(self):
        return self._category_model.get_selection_model()

    def get_selection_model(self):
        return self._selection_model

    def __init__(self, parent):
        self._parent = parent
        self._category_model = DeltaCategoryModel(self)
        base_model = DeltaBaseModel(self)
        filter_model = DeltaFilterModel.new(self, base_model)
        self._selection_model = Gtk.MultiSelection.new(filter_model)

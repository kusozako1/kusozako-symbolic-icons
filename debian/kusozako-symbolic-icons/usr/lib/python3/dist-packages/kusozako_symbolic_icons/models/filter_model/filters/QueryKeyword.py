# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_symbolic_icons.const import ApplicationSignals


class DeltaQueryKeyword(DeltaEntity):

    def matches(self, file_info):
        name = file_info.get_name()
        return self._keyword in name

    def has_keyword(self):
        return self._keyword != ""

    def receive_transmission(self, user_data):
        signal, keyword = user_data
        if signal != ApplicationSignals.QUERY_KEYWORD_CHANGED:
            return
        self._keyword = keyword
        self._raise("delta > filter changed")

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
        self._raise("delta > register application object", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaCategoryModel(Gio.ListStore, DeltaEntity):

    def get_selection_model(self):
        return Gtk.SingleSelection.new(self)

    def append_category(self, category):
        item = Gtk.StringObject.new(category)
        self.append(item)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gtk.StringObject)
        item = Gtk.StringObject.new("All")
        self.append(item)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_symbolic_icons.const import ExtraMenuPages


class DeltaProxySetter(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, signal_param = user_data
        if signal != MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM:
            return
        page_name, file_info = signal_param
        if page_name == ExtraMenuPages.COPY_TO_CLIPBOARD:
            self._raise("delta > proxy changed", file_info)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)

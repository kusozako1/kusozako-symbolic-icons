# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako1.widget.overlay_item.Separator import DeltaSeparator
from kusozako_symbolic_icons.const import ExtraMenuPages
from .ExitButton import DeltaExitButton
from .CopyIconName import DeltaCopyIconName
from .CopyAbsolutePath import DeltaCopyAbsolutePath


class DeltaContentArea(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(propagate_natural_width=True)
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=False,
            )
        self.set_size_request(320, -1)
        self.add_css_class("osd")
        DeltaExitButton(self)
        DeltaSeparator(self)
        DeltaCopyIconName(self)
        DeltaCopyAbsolutePath(self)
        scrolled_window.set_child(self)
        param = scrolled_window, ExtraMenuPages.COPY_TO_CLIPBOARD
        user_data = MainWindowSignals.ADD_EXTRA_PRIMARY_MENU, param
        self._raise("delta > main window signal", user_data)

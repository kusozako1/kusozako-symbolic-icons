# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaExitButton(AlfaButton):

    START_ICON = "application-exit-symbolic"
    LABEL = _("Exit Menu")

    def _on_clicked(self, button):
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

    def _on_map(self, button):
        button.grab_focus()

    def _on_initialize(self):
        self.connect("map", self._on_map)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.main_loop.MainLoop import AlfaMainLoop
from kusozako1.Transmitter import FoxtrotTransmitter
from . import APPLICATION_DATA
from .models.Models import DeltaModels
from .extra_menus.ExtraMenus import EchoExtraMenus
from .user_interface.UserInterface import DeltaUserInterface


class DeltaMainLoop(AlfaMainLoop):

    def _delta_info_model(self):
        return self._models.get_selection_model()

    def _delta_info_category_model(self):
        return self._models.get_category_model()

    def _delta_call_register_application_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_application_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_loopback_application_window_ready(self, parent):
        self._transmitter = FoxtrotTransmitter()
        self._models = DeltaModels(parent)
        EchoExtraMenus(parent)
        DeltaUserInterface(parent)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)

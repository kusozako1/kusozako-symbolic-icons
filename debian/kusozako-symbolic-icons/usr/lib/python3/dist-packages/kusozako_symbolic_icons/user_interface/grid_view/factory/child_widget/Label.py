# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import PangoCairo
from kusozako1.Entity import DeltaEntity
from kusozako1.util.LayoutFactory import FoxtrotLayoutFactory

SHADE_COLOR_RGBA = 0, 0, 0, 0.4
r = 12


class DeltaLabel(Gtk.DrawingArea, DeltaEntity):

    def _draw_func(self, drawing_area, cairo_context, width, height, name):
        layout = self._layout_factory.build_(cairo_context, 128, 128/2)
        layout.set_markup(GLib.markup_escape_text(name, -1))
        _, layout_height = layout.get_pixel_size()
        cairo_context.set_source_rgba(0, 0, 0, 0.5)
        _, layout_height = layout.get_pixel_size()
        cairo_context.set_source_rgba(*SHADE_COLOR_RGBA)
        x = 0
        rectangle_height = layout_height+4*2
        y = height-rectangle_height
        cairo_context.arc(x, y, 0, 0, 0)
        cairo_context.arc(x+width, y, 0, 0, 0)
        cairo_context.arc(x+width-r, y+rectangle_height-r, r, 0, math.pi/2)
        cairo_context.arc(x+r, y+rectangle_height-r, r, math.pi/2, math.pi)
        cairo_context.close_path()
        cairo_context.fill()
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(4, 128-layout_height-4)
        PangoCairo.update_layout(cairo_context, layout)
        PangoCairo.show_layout(cairo_context, layout)

    def bind(self, file_info):
        filename = file_info.get_name()
        stem = filename.replace(".svg", "")
        self.set_draw_func(self._draw_func, stem)
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        self._layout_factory = FoxtrotLayoutFactory()
        Gtk.DrawingArea.__init__(self)
        self._raise("delta > add overlay", self)

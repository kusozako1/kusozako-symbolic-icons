# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_symbolic_icons.const import ApplicationSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_changed(self, entry):
        keyword = entry.get_text()
        user_data = ApplicationSignals.QUERY_KEYWORD_CHANGED, keyword
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(
            self,
            placeholder_text=_("Search"),
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            )
        self.set_size_request(-1, 32)
        self.connect("changed", self._on_changed)
        self._raise("delta > add to container", self)

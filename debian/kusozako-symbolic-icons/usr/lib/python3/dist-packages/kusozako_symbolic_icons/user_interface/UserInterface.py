# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .side_pane.SidePane import DeltaSidePane
from .grid_view.GridView import DeltaGridView


class DeltaUserInterface(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_child(widget)

    def _delta_call_add_to_container_end(self, widget):
        self.set_end_child(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Paned.__init__(self, hexpand=True, vexpand=True, wide_handle=True)
        self.set_position(200)
        DeltaSidePane(self)
        DeltaGridView(self)
        self._raise("delta > add to container", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaCopyAbsolutePath(AlfaButton):

    LABEL = _("Copy Absolute Path")

    def _on_clicked(self, button):
        file_info = self._enquiry("delta > proxy")
        path = file_info.get_attribute_string("kusozako1::path")
        display = Gdk.Display.get_default()
        clipboard = display.get_clipboard()
        clipboard.set(path)
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

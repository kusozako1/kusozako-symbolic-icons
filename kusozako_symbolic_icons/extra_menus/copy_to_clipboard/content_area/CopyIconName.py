# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako1.const import MainWindowSignals


class DeltaCopyIconName(AlfaButton):

    LABEL = _("Copy Icon Name")

    def _on_clicked(self, button):
        file_info = self._enquiry("delta > proxy")
        name = file_info.get_name()
        display = Gdk.Display.get_default()
        clipboard = display.get_clipboard()
        clipboard.set(name.split(".", 1)[0])
        user_data = MainWindowSignals.CLOSE_OVERLAY, None
        self._raise("delta > main window signal", user_data)

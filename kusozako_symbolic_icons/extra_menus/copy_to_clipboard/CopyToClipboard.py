# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from .ProxySetter import DeltaProxySetter
from .content_area.ContentArea import DeltaContentArea


class DeltaCopyToClipboard(DeltaEntity):

    def _delta_info_proxy(self):
        return self._file_info

    def _delta_call_proxy_changed(self, file_info):
        self._file_info = file_info

    def __init__(self, parent):
        self._parent = parent
        DeltaProxySetter(self)
        DeltaContentArea(self)

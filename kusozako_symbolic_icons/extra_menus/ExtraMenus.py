# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .copy_to_clipboard.CopyToClipboard import DeltaCopyToClipboard


class EchoExtraMenus:

    def __init__(self, parent):
        DeltaCopyToClipboard(parent)

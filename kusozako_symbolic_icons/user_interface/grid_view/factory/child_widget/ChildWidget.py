# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_symbolic_icons.const import ExtraMenuPages
from .Image import DeltaImage
from .Label import DeltaLabel


class DeltaChildWidget(Gtk.AspectFrame, DeltaEntity):

    def _on_released(self, gesture, n_press, x, y):
        param = ExtraMenuPages.COPY_TO_CLIPBOARD, self._file_info
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU_WITH_PARAM, param
        self._raise("delta > main window signal", user_data)

    def bind(self, file_info):
        self._image.bind(file_info)
        self._label.bind(file_info)
        self._file_info = file_info

    def _delta_call_add_to_container(self, widget):
        self._overlay.set_child(widget)

    def _delta_call_add_overlay(self, widget):
        self._overlay.add_overlay(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.AspectFrame.__init__(self, ratio=1, margin_top=8, margin_start=8)
        gesture_click = Gtk.GestureClick(button=Gdk.BUTTON_SECONDARY)
        gesture_click.connect("released", self._on_released)
        self.add_controller(gesture_click)
        self._overlay = Gtk.Overlay()
        self.set_child(self._overlay)
        self._image = DeltaImage(self)
        self._label = DeltaLabel(self)
        self._overlay.add_css_class("card")

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity


class DeltaImage(Gtk.Image, DeltaEntity):

    def bind(self, file_info):
        path = file_info.get_attribute_string("kusozako1::path")
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(path, 96, 96)
        self.set_from_pixbuf(pixbuf)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(
            self,
            pixel_size=96,
            margin_top=16,
            margin_bottom=16,
            margin_start=16,
            margin_end=16,
            )
        self._raise("delta > add to container", self)

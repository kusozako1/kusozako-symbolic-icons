# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_symbolic_icons.const import ApplicationSignals
from .Factory import DeltaFactory


class DeltaListView(Gtk.ListView, DeltaEntity):

    def _on_activate(self, list_view, index):
        model = list_view.get_model()
        item = model[index]
        category = item.get_string()
        user_data = ApplicationSignals.CATEGORY_CHANGED, category
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(hexpand=True, vexpand=True)
        Gtk.ListView.__init__(
            self,
            model=self._enquiry("delta > category model"),
            factory=DeltaFactory(self),
            single_click_activate=True,
            )
        self.connect("activate", self._on_activate)
        self.add_css_class("kusozako-osd")
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)

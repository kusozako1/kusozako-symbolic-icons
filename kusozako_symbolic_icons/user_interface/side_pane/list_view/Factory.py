# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class DeltaFactory(Gtk.SignalListItemFactory, DeltaEntity):

    def _on_setup(self, factory, list_item):
        label = Gtk.Label(xalign=0, margin_start=8)
        label.set_size_request(-1, 32)
        list_item.set_child(label)

    def _on_bind(self, factory, list_item):
        label = list_item.get_child()
        string_object = list_item.get_item()
        label.set_label(string_object.get_string())

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListItemFactory.__init__(self)
        self.connect("setup", self._on_setup)
        self.connect("bind", self._on_bind)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .filters.Filters import DeltaFilters


class DeltaFilterModel(Gtk.FilterListModel, DeltaEntity):

    @classmethod
    def new(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.FilterListModel.__init__(
            self,
            filter=DeltaFilters(self)
            )

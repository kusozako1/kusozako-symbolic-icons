# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_symbolic_icons.const import ApplicationSignals


class DeltaCategory(DeltaEntity):

    def matches(self, file_info):
        if self._category == "All":
            return True
        category = file_info.get_attribute_string("kusozako1::category")
        return category == self._category

    def receive_transmission(self, user_data):
        signal, category = user_data
        if signal != ApplicationSignals.CATEGORY_CHANGED:
            return
        self._category = category
        self._raise("delta > filter changed")

    def __init__(self, parent):
        self._parent = parent
        self._category = "All"
        self._raise("delta > register application object", self)

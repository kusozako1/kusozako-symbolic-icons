# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .QueryKeyword import DeltaQueryKeyword
from .Category import DeltaCategory


class DeltaFilters(Gtk.CustomFilter, DeltaEntity):

    def _filter_func(self, file_info):
        if self._query_keyword.has_keyword():
            return self._query_keyword.matches(file_info)
        return self._category.matches(file_info)

    def _delta_call_filter_changed(self):
        self.changed(Gtk.FilterChange.DIFFERENT)

    def __init__(self, parent):
        self._parent = parent
        self._query_keyword = DeltaQueryKeyword(self)
        self._category = DeltaCategory(self)
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._filter_func)

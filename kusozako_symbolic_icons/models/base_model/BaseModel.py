# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .Parser import DeltaParser


class DeltaBaseModel(Gio.ListStore, DeltaEntity):

    def _delta_call_file_found(self, user_data):
        fullpath, file_info = user_data
        dirname = GLib.path_get_dirname(fullpath)
        category = GLib.path_get_basename(dirname)
        file_info.set_attribute_string("kusozako1::category", category)
        file_info.set_attribute_string("kusozako1::path", fullpath)
        self.append(file_info)

    def _delta_call_directory_found(self, fullpath):
        DeltaParser.new(self, fullpath)
        category = GLib.path_get_basename(fullpath)
        self._raise("delta > category found", category)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.FileInfo)
        DeltaParser.new(self, "/usr/share/icons/Adwaita/symbolic")
